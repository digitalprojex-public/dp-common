import * as moment from 'moment';
import 'moment/locale/es';

moment.locale('es');

/**
 * 'YYYY-MM-DDThh:mm:00' Formato de fecha para comunicarce con el servidor
 * "DD/MM/YYYY"     Formato para mostrar a los usuarios
 * 'YYYY-MM-DD'     Formatos que se mandan los filtros
 */
export class DateTools {
    static fromString(date: string, format: string = 'YYYY-MM-DDThh:mm:00'): moment.Moment {
        return moment(date, 'YYYY-MM-DDThh:mm:00');
    }

    static formatStringDate(date: string, format: string = 'YYYY-MM-DD') {
        return moment(date).format(format);
    }

    static formatDateISO(date: moment.Moment): string {
        return date.format('YYYY-MM-DDTHH:mm:00');
    }

    static formatDateFilter(date: moment.Moment): string {
        return date.format('YYYY-MM-DD');
    }

    static toTime(date: string) {
        return moment(date).format('hh:mm A');
    }
    static toTimeTwo(date: string) {
        return moment(date).format('hh:mm');
    }

    static startOfMonth(month: number, year: number): moment.Moment {
        return moment().year(year).month(month).startOf('month');
    }

    static endOfMonth(month: number, year: number): moment.Moment {
        return moment().year(year).month(month).endOf('month');
    }

    static addYears(count: number): string {
        return moment().add(count, 'y').format();
    }

    static createDate(day: number, month: number, year: number): moment.Moment {
        return moment().year(year).month(month).date(day);
    }


    static getMonthNames() {
        return 'Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre';
    }

    static getMonthShortNames() {
        return 'Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic';
    }

    getMonthNames() {
        return DateTools.getMonthNames();
    }

    getMonthShortNames() {
        return DateTools.getMonthShortNames();
    }
}
