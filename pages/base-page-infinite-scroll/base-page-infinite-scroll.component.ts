import {Component, ViewChild} from '@angular/core';
import {LoadingController} from '@ionic/angular';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {InfiniteScrollComponent} from '../../components/infinite-scroll/infinite-scroll.component';
import {DpBasePageComponent} from '../dp-base-page/dp-base-page.component';
import {ResultApi} from '../../models/result-api';
import {DpAlertService} from '../../services/dp-alert.service';

@Component({
    selector: 'app-base-page-infinite-scroll',
    template: ''
})
export class DpBasePageInfiniteScrollComponent extends DpBasePageComponent {
    @ViewChild(InfiniteScrollComponent, {static: false}) infiniteScroll: InfiniteScrollComponent;
    items: any[];
    perPage = 8;
    page = 1;

    constructor(protected alertService: DpAlertService,
                protected activatedRoute: ActivatedRoute,
                protected loadingController: LoadingController) {
        super(alertService, loadingController);
        this.items = [];
    }

    initializeData() {
        this.page = 1;
        this.items = [];
        this.isLoading = true;
    }

    ionViewDidEnter() {
    }


    // tslint:disable-next-line:use-lifecycle-interface
    ngOnInit() {
        super.ngOnInit();
        this.activatedRoute.queryParamMap.subscribe((paramMap: ParamMap) => {
            const refresh = paramMap.get('refresh');
            if (refresh || this.items.length === 0) {
                this.initializeData();
                this.loadData();
            }
        });
    }

    ionInfinite() {
        if (!this.infiniteScroll.disabled) {
            this.loadData();
        }
    }

    processResult(resultApi: ResultApi<any>) {
        if (resultApi.success) {
            this.items.push(...resultApi.items);
            if (this.items.length) {
                this.page++;
            }
        } else {
            this.presentError(resultApi.error);
        }

        this.infiniteScroll.complete();
        this.isLoading = false;
    }
}
