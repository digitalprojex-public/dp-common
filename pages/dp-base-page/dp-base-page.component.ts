import {Component, OnInit} from '@angular/core';
import {LoadingController, ToastController} from '@ionic/angular';
import {DpAlertService} from '../../services/dp-alert.service';

@Component({
    selector: 'app-base-page',
    template: ''
})
export class DpBasePageComponent implements OnInit {
    isLoading = true;
    pageTitle: string;
    pageSubTitle: string;
    loading: HTMLIonLoadingElement;

    constructor(protected alertService: DpAlertService, protected loadingController: LoadingController) {
    }

    ngOnInit() {
    }

    ionViewDidEnter() {
        this.isLoading = true;
        this.loadData();
    }

    loadData() {
        throw new Error('loadData not implemented');
    }

    async presentLoading(msg) {
        this.loading = await this.loadingController.create({
            message: msg,
            cssClass: 'custom-loading',
        });
        await this.loading.present();
    }

    async hideLoading() {
        if (this.loading) {
            await this.loading.dismiss();
        }
    }

    presentError(msg) {
        this.alertService.presentError(msg);
    }

    presentSuccess(msg) {
        this.alertService.presentSuccess(msg);
    }
}
