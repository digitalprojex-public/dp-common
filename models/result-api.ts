import {Model} from './model';

export class ResultApi<T extends Model> {
    success: boolean;
    items: T[];
    error: string;
    count: number;
}
