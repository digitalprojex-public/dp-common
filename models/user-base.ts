export interface UserBase {
    token: string;
    /**
     * Para guardarlo el current user en el local storage
     */
    toStorage();
}

