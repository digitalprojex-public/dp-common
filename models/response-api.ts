export class ResponseApi {
    data: any;
    count: number;
    error?: {
        code: string,
        message: string
    };
    messages?: {
        code: number,
        message: string
    };
}
