export class ResponseMe {
    UserId: string;
    UserLastName: string;
    UserFirstName: string;
    UserEMail: string;
    UserPhone: string;
    UserActive: boolean;
}
