export abstract class Model {
    id: string;
    abstract fromJSON(data);
}
