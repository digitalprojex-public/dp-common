import {ResponseApi} from '../../dp-common/models/response-api';

export class ResponseLogin extends ResponseApi {
    access_token: string;
    scope: string;
    refresh_token: string;
    user_guid: string;
}
