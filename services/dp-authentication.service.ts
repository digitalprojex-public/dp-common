import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {NavController, Platform, ToastController} from '@ionic/angular';
import {BehaviorSubject, from, Observable, throwError} from 'rxjs';
import {map, switchMap, take} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {IDpAuthenticationService} from './i-dp-authentication.service';
import {UserBase} from '../models/user-base';

@Injectable({
    providedIn: 'root'
})
export class DpAuthenticationService<T extends UserBase> implements IDpAuthenticationService {
    public user: Observable<any>;
    protected userData = new BehaviorSubject(null);

    CURRENT_USER_KEY = 'current_user';
    ROUTES_HOME = '/login';
    authURL = '/oauth/access_token';

    constructor(
        protected navCtrl: NavController,
        protected router: Router,
        protected httpClient: HttpClient,
        protected storage: Storage,
        protected toastController: ToastController,
        protected plt: Platform
    ) {
        this.loadStoredToken();
    }

    loadStoredToken() {
        const platformObs = from(this.plt.ready());

        this.user = platformObs.pipe(
            switchMap(() => {
                return from(this.storage.get(this.CURRENT_USER_KEY));
            }),
            map(user => {
                if (user) {
                    this.userData.next(user);
                    return true;
                } else {
                    return null;
                }
            })
        );
    }


    login(userName: string, userPass: string): Observable<boolean> {
        throw new Error('login not implemented');
    }

    protected handleError(error: HttpErrorResponse) {
        console.error(error.statusText);
        let status = error.statusText;
        if (status == "Unknown Error") {
            status = "Ups tenemos problemas al conectar con el servidor!!!"
        }
        return throwError(status);
    }

    logout() {
        return this.storage.remove(this.CURRENT_USER_KEY).then(() => {
            this.router.navigateByUrl(this.ROUTES_HOME);
            this.userData.next(null);
        });
    }

    isAuthenticated(): Observable<boolean> {
        return this.user.pipe(
            take(1),
            map(user => {
                if (!user) {
                    this.router.navigateByUrl(this.ROUTES_HOME);
                    return false;
                } else {
                    return true;
                }
            })
        );
    }

    getToken(): string {
        let token = null;
        if (this.getCurrentUser()) {
            token = this.getCurrentUser().token;
        }
        return token;
    }

    setCurrentUser(user: T = null): Promise<any> {
        this.userData.next(user);
        if (user) {
            return this.storage.set(this.CURRENT_USER_KEY, user.toStorage());
        } else {
            return this.storage.remove(this.CURRENT_USER_KEY);
        }
    }

    getCurrentUser(): T {
        return this.userData.getValue();
    }
}
