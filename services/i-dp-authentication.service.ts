import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export abstract class IDpAuthenticationService {
    user: Observable<any>;
    abstract isAuthenticated(): Observable<boolean>;
    abstract getToken(): string;
    abstract login(userName: string, userPass: string): Observable<boolean> ;
    abstract logout();
    abstract getCurrentUser(): any;
    abstract setCurrentUser(user: any);
}
