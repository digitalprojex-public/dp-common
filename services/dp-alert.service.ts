import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class DpAlertService {

    constructor(protected toastController: ToastController) {
    }

    public async presentError(msg) {
        this.presentAlert(msg, 'danger', 'top', true);
    }

    public async presentSuccess(msg) {
        this.presentAlert(msg, 'success', 'top');
    }

    public async presentAlert(msg,
                              color = 'primary',
                              position: 'top' | 'bottom' | 'middle' = 'top',
                              closeButtons = true,
                              header = '',
                              buttons = [],
                              duration = 5000,
    ) {
        if (closeButtons) {
            buttons.push({icon: 'close', role: 'cancel'});
        }

        const toast = await this.toastController.create({
            header,
            message: msg,
            color,
            duration,
            position,
            buttons,
            keyboardClose: true
        });
        toast.present();
    }
}
