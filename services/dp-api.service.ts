import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable, throwError, of as observableOf} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ResultApi} from '../models/result-api';
import {ResponseApi} from '../models/response-api';
import {Model} from '../models/model';

// @Injectable({
//     providedIn: 'root'
// })
export class DpApiService<T extends Model> {

    constructor(protected httpClient: HttpClient,
                protected ctor: new () => T,
                protected resource: string) {
    }

    /**
     * Get one element.
     * @param id Filter element
     * @param idKey If identify key not "id"
     */
    getItem(id: string, idKey: string = 'id'): Observable<T> {
        const params = new HttpParams()
            .set(idKey, id);

        return this.httpClient.get<any>(this.getUrl('get'), {params})
            .pipe(
                map((response) => {
                    const model = new this.ctor();
                    try {
                        model.fromJSON(response.data);
                    } catch (e) {
                        throwError('Data not found.');
                    }
                    return model;
                }), catchError(this.handleError)
            );
    }

    /**
     * Get list of elemnts.
     * @param url Api rest URI
     * @param params Filter data
     */
    getItems(url: string, params: HttpParams = new HttpParams()): Observable<ResultApi<T>> {
        return this.httpClient.get<ResponseApi>(url, {params})
            .pipe(
                map((response: ResponseApi) => {
                    let success = true;
                    const result = new ResultApi<T>();

                    result.items = [];
                    if (response.error) {
                        success = false;
                        result.error = response.error.message;
                    } else {
                        success = true;
                        const items: T[] = [];
                        let count = 0;
                        if (response.data) {
                            response.data.forEach((data: any, index: number) => {
                                const model = new this.ctor();
                                model.fromJSON(data);
                                items.push(model);
                            });
                            count = response.count;
                        }

                        result.items = items;
                        result.count = count;
                    }
                    result.success = success;
                    return result;

                }), catchError(this.handleError)
            );
    }

    /**
     * Get list of api
     * @param page Current page
     * @param perPage Count element per page
     * @param extraParams Aditional parameter for filter
     * @param urlKey If url is not by default
     */
    getList(page: number = 1, perPage: number = 10,
            extraParams: { [key: string]: string } = {},
            urlKey = 'list'): Observable<ResultApi<T>> {
        let params = new HttpParams()
            .set('Pagesize', perPage.toString())
            .set('Pagenumber', page.toString());

        for (const key in extraParams) {
            if (extraParams.hasOwnProperty(key)) {
                const value = extraParams[key];
                params = params.set(key, value);
            }
        }

        return this.getItems(this.getUrl(urlKey), params);
    }

    /**
     * Insert new element.
     * @param body Data to insert.
     */
    newItem(body: any): Observable<T> {
        return this.httpClient.post<ResponseApi>(this.getUrl('insert'), body)
            .pipe(
                map((response: ResponseApi) => this.processSave(response)),
                catchError(this.handleError)
            );
    }

    /**
     * Modify element.
     * @param body Data to send.
     */
    updateItem(body: any): Observable<T> {
        return this.httpClient.post<ResponseApi>(this.getUrl('update'), body)
            .pipe(
                map((response: ResponseApi) => this.processSave(response))
                , catchError(this.handleError)
            );
    }

    /**
     * Remove elemnt.
     * @param id The identify elemnt for delete.
     */
    deleteItem(id: string): Observable<boolean> {
        return this.httpClient.post<ResponseApi>(this.getUrl('delete'), {id})
            .pipe(
                map((response: ResponseApi) => {
                    return true;
                })
                , catchError(this.handleError)
            );
    }

    /**
     * Verify and transform response from update element.
     * @param response Response from update element.
     */
    protected processSave(response: ResponseApi) {
        const model = new this.ctor();
        if (response.error) {
            throw throwError(response.error.message);
        }
        if (!response.messages || response.messages.code === 200) {
            try {
                model.fromJSON(response.data);
            } catch (e) {
                throw throwError('Data not found.');
            }
        } else {
            throw throwError(response.messages.message);
        }

        return model;
    }

    /**
     * Return url from key.
     * @param type Key to url.
     */
    protected getUrl(type: string) {
        const urls = {
            list: '/' + this.resource + '/list',
            get: '/' + this.resource + '/get',
            insert: '/' + this.resource + '/insert',
            update: '/' + this.resource + '/update',
            delete: '/' + this.resource + '/delete'
        };
        return urls[type];
    }

    /**
     * Throw error for response
     * @param error Response with error.
     */
    protected handleError(error: HttpErrorResponse) {
        if (error instanceof Observable) {
            return error;
        }
        if (error.error && error.error.error) {
            return throwError(error.error.error.message);
        }
        return throwError(error.statusText);
    }
}
