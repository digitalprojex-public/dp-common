import {Injectable} from '@angular/core';
import {CanLoad, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {IDpAuthenticationService} from './i-dp-authentication.service';

@Injectable({
    providedIn: 'root'
})
export class DpAuthGuardService implements CanLoad {
    constructor(protected authenticationService: IDpAuthenticationService,
                protected router: Router) {
    }

    canLoad(): Observable<boolean> | Promise<boolean> | boolean {
        return this.authenticationService.isAuthenticated();
    }

    /* NO usar can activate, da problemas con las aplicaciones cuando se cierran y vuelven a abrir
    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
         return this.authenticationService.isAuthenticated();
     }*/
}
