# DPCommon
Modulo de Angular para Ionic con funcionalidades comunes para varios proyectos.

# Clonar en un proyecto 
```shell script
git submodule add ../android/dp-common.git src/app/dp-common
```

"../android/dp-common.git" es la direccion relativa en el gitlab.com, este es un ejeplo cuando estan en el mismo grupo.

# Use
## Integrar con la app
Agregar los siguientes provders a **app.module.ts**

```typescript
...
 providers: [
      ...
        {
            provide: HTTP_INTERCEPTORS,
            useClass: BaseUrlInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HeaderInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: GxErrorInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        {
            provide: IDpAuthenticationService,
            useClass: AuthenticationService,
        }
        ],
...
```
## Models
Cada modelo debe heredar de "../../dp-common/models/model" e implementar el emtodo **fromJSON** para cuando se 
recibe de la api y **toJSON** si se envia a la api.
ejemplo 
```typescript
import {Model} from "../../dp-common/models/model";

export class Client implements Model {
    id: string;
    name: string;
    firstName: string;
    lastName: string;

    fromJSON(data) {
        this.id = data.Clientid;
        this.name = data.Clientfullname;
        this.firstName = data.Clientfirstname;
        this.lastName = data.Clientlastname;
    }

    toJSON() {
        let json = {
            Clientfirstname: this.firstName,
            Clientlastname: this.lastName,
        };

        if(this.id){
            json['Clientid']= this.id;
        }
        return json;
    }
}
```

## Services
Los servicios deben de heredar de **DpApiService<T>** y establecer el modelo
ejemplo:

```typescript
import {Injectable} from '@angular/core';
import {DpApiService} from "../../dp-common/services/dp-api.service";
import {HttpClient} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import {Client} from "../models/client";

@Injectable({
    providedIn: 'root'
})
export class ClientService extends DpApiService<Client> {

    constructor(protected httpClient: HttpClient,
                protected storage: Storage) {
        super(httpClient, Client, 'clients');
    }
}
```

## Usar servicios
Para usar los servicios en una pagina agregar el servicio y utilizar los metodos
Ejemplo de listar:

```typescript
import {Component, OnInit} from '@angular/core';
import {ClientService} from "../../../core/services/client.service";
import {Client} from "../../../core/models/client";
import {ResultApi} from "models/result-api";

@Component({
    selector: 'app-clients-list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.scss'],
})
export class ListClientsPage implements OnInit {
    items: Client[];
    search: string;

    constructor( private clientsService: ClientService) {       
    }

    ngOnInit() {
       
        let extraParams = {};
        if (this.search && this.search != '') {
            extraParams = {Filter: this.search};
        }
        this.clientsService.getList(this.page, this.perPage, extraParams).subscribe(
            (result: ResultApi<Client>) => {
                        if (result.success) {
                            this.items.push(...result.items);
                            if (this.items.length) {
                                this.page++;
                            }
                        } else {
                            // ocurrio un error en "resultApi.error" descripcion del error
                        }
            });
    }
}
```

Ejemplo de Editar:

```typescript
import {Component,OnInit} from '@angular/core';
import {Client} from "../../../core/models/client";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ClientService} from "../../../core/services/client.service";

@Component({
    selector: 'app-edit',
    templateUrl: './edit.page.html',
    styleUrls: ['./edit.page.scss'],
})
export class EditClientPage implements OnInit  {
    client: Client;
    Object = Object;
    pageTitle = "Editar Cliente";
    clientForm: FormGroup;

    constructor( protected clientService: ClientService,
                private formBuilder: FormBuilder,
    ) {
    }

    ngOnInit() {
        this.clientForm = this.formBuilder.group({
            firstName: [this.client.firstName, [Validators.required]],
            lastName: [this.client.lastName, [Validators.required]]
        }, {});
    }

    onSubmit(): void {
        // Stop if the form validation has failed
        if (this.clientForm.invalid) {
            return;
        }
        const values = this.clientForm.value;
        Object.keys(values).forEach(key => this.client[key] = values[key]);

        let clientJSON = this.client.toJSON();
        delete clientJSON[''];
        this.clientService.updateItem(clientJSON).subscribe(
            (client: Client) => {
                this.client = client;
            },
            error => {
                //ocurrio un erron con mensaje en "error"
            }
        );
    }
}
```

Ejemplo de nuevo elemnto:

```typescript
import {Component,OnInit} from '@angular/core';
import {Client} from "../../../core/models/client";
import {ClientService} from "../../../core/services/client.service";
import {FormBuilder, FormGroup, FormGroupDirective, Validators} from "@angular/forms";

@Component({
    selector: 'app-new',
    templateUrl: './new.page.html',
    styleUrls: ['./new.page.scss'],
})
export class NewClientPage implements OnInit {
    client: Client;
    clientForm: FormGroup;

    constructor( protected clientService: ClientService,
                private formBuilder: FormBuilder,
    ) {
        this.client = new Client();
    }

    ngOnInit() {
        this.clientForm = this.formBuilder.group({
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]]
        }, {});
    }

    onSubmit(): void {
        // Stop if the form validation has failed
        if (this.clientForm.invalid) {
            return;
        }

        const values = this.clientForm.value;
        Object.keys(values).forEach(key => this.client[key] = values[key]);

        this.clientService.newItem(this.client.toJSON()).subscribe(
            (client: Client) => {
                    // en client esta el cliente nuevo                
            },
            error => {
                //ocurrio un erron con mensaje en "error"
            }
        );
    }
}
```

## Pages 
Se muede ustilizar las paginas bases 

### DpBasePageInfiniteScrollComponent

```typescript
import {Component, OnInit} from '@angular/core';
import {ToastController} from "@ionic/angular";
import {ClientService} from "../../../core/services/client.service";
import {Client} from "../../../core/models/client";
import {DpBasePageInfiniteScrollComponent} from "../../../dp-common/pages/base-page-infinite-scroll/base-page-infinite-scroll.component";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {ResultApi} from "../../../dp-common/models/result-api";

@Component({
    selector: 'app-clients-list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.scss'],
})
export class ClientListPage extends DpBasePageInfiniteScrollComponent {
    items: Client[];
    search: string;
    pageTitle = 'Clientes';

    constructor(
        protected toastController: ToastController,
        private clientsService: ClientService,
        private router: Router,
        protected activatedRoute: ActivatedRoute) {
        super(toastController, activatedRoute);
    }

    ionViewDidEnter() {
        this.page = 1;
        this.items = [];
        super.ionViewDidEnter();
    }

    loadData() {
        let extraParams = {};
        if (this.search && this.search != '') {
            extraParams = {Filter: this.search};
        }
        this.clientsService.getList(this.page, this.perPage, extraParams).subscribe(
            (result: ResultApi<Client>) => {
                this.processResult(result);
            });
    }

    viewDetails(client: Client) {
        this.router.navigateByUrl('/clients/view', {replaceUrl: true, state: client});
    }

    searchEventFired() {
        this.items = [];
        this.loadData();
    }

    addClient() {
        this.router.navigateByUrl('/clients/new', {replaceUrl: true});
    }
}
```

## Login 

Para el sistema de login con el GAM
1. Definir le servicio **auth-guard.service.ts**

```typescript
import {Injectable} from '@angular/core';
import {DpAuthGuardService} from '../dp-common/services/dp-auth-guard.service';
import {IDpAuthenticationService} from '../dp-common/services/i-dp-authentication.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService extends DpAuthGuardService {
    constructor(public authenticationService: IDpAuthenticationService) {
        super(authenticationService);
    }
}
```

2. Definir le servicio **authentication.service.ts**, se puede redefinir el login si no es igual al implementado.

```typescript
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {NavController, ToastController} from '@ionic/angular';
import {HttpClient, } from '@angular/common/http';
import {DpAuthenticationService} from '../dp-common/services/dp-authentication.service';
import {User} from '../models/user';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService extends DpAuthenticationService<User> {

    authURL = '/oauth/access_token';
    constructor(
        protected navCtrl: NavController,
        protected router: Router,
        protected httpClient: HttpClient,
        protected storage: Storage,
        public toastController: ToastController
    ) {
        super(navCtrl, router, httpClient, storage, toastController);
    }
    
}
```

3. Agregar el provider de **IDpAuthenticationService** al **app.module.ts**
```
        ...
        {
            provide: IDpAuthenticationService,
            useClass: AuthenticationService,
        }
        ...
```

4. En el **app-routing.module.ts** usar l aclase **services/auth-guard.service**


# Requisitos de la API de GX
1. Las url de un recurso para el crud tiene que tener el siguiente fomato

    - list: '/resource/list',
    - get: '/resource/get',
    - insert: '/resource/insert',
    - update: '/resource/update',
    - delete: '/resource/delete'

    Donde **resource** debe sustituirse por el nombre del recurso, 
    ejmplo por **clients**

2. Los parametros del paginado son "Pagesize", "Pagenumber"
3. La respuesta de los listados siempre debe ser:

    ```json
    {
        "data": [{"id":1, "name":"Manolo"}, {"id":2, "name":"Juan"}],
        "count": 10,
        "error":{
            "code": "403",
            "message": "No tiene permisos"
        }
    }
    ```
4. **error**: Se envia el solo cuando existe algun error.  

5.  En el **actualizar** y **agregar** se envia la respuesta:
    ```json
    {
        "data": {"id":1, "name":"Manolo"},
        "error":{
            "code": "403",
            "message": "No tiene permisos"
        }
    }
    ```

6.  En el **obtener un elemento** el parametro identificador es 'id'

6.  En el **obtener un elemento** se envia la respuesta:
    ```json
    {
        "id":1, 
        "name":"Manolo"
    }
    ```

7. En el eliminar se debe enviar solo el error si existe sino se envia vacio:
    ```json
    {
        "error":{
            "code": "403",
            "message": "No tiene permisos"
        }
    }
    ```





