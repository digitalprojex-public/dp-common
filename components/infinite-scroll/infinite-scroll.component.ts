import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {IonInfiniteScroll} from '@ionic/angular';

@Component({
    selector: 'app-infinite-scroll',
    templateUrl: './infinite-scroll.component.html',
    styleUrls: ['./infinite-scroll.component.scss'],
})
export class InfiniteScrollComponent implements OnInit {
    @Output() infinite = new EventEmitter();
    @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;
    @Input() disabled: boolean;

    constructor() {
    }

    ngOnInit() {
    }

    ionInfinite() {
        this.infinite.emit();
    }

    complete() {
        this.infiniteScroll.complete();
    }
}
