import { NgModule } from '@angular/core';
import {DpBasePageComponent} from './pages/dp-base-page/dp-base-page.component';
import {DpBasePageInfiniteScrollComponent} from './pages/base-page-infinite-scroll/base-page-infinite-scroll.component';
import {LoadingComponent} from './components/loading/loading.component';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [DpBasePageComponent, DpBasePageInfiniteScrollComponent, LoadingComponent],
    imports: [
        CommonModule,
        IonicModule
    ],
  exports: [DpBasePageComponent, DpBasePageInfiniteScrollComponent, LoadingComponent]
})
export class DpCommonModule { }
