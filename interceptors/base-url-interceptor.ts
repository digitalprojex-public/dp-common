import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment as ENV} from '../../../environments/environment';

@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // skip requests for local assets
        // maybe put extra condition to allow for multiple API usage
        if (request.url.indexOf('assets/') >= 0) {
            return next.handle(request);
        }

        const url = this.getUrl(request.url);
        let params = request.params;

        if (request.method === 'GET') {
            params = params.set('_time', (new Date()).getTime().toString());
        }

        const apiReq = request.clone({
            url,
            params
        });

        return next.handle(apiReq);
    }

    /**
     * Devuelve al url completa de un servicio obteniendo los parametros del enviroment en el fichero
     * "src/environments/environment.prod.ts"
     *
     * @param uri Parte de la url desde de la base.
     *            Empiesa siempre con /
     *            Si quiere que no se agrege el baseURL a la ure final empezar con "./"
     */
    private getUrl(uri: string): string {
        let url: string;
        if(uri.startsWith('.')){
            url = ENV.api.protocol + '://' + ENV.api.host + uri.substring(1);
        }else{
            url = ENV.api.protocol + '://' + ENV.api.host + ENV.api.baseURL + uri;
        }
        return url;
    }
}
