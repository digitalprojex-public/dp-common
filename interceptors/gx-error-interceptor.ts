import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class GxErrorInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    if (event.body.error && event.body.error.code) {
                        throw new HttpErrorResponse(
                            {
                                error: event.body.error.message,
                                headers: event.headers,
                                status: event.body.error.code,
                                statusText: event.body.error.message,
                                url: event.url
                            }
                        );
                    }
                }
                return event;
            })
        );
    }
}
