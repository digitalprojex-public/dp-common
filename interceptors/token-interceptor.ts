import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IDpAuthenticationService} from '../services/i-dp-authentication.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private auth: IDpAuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.auth.getToken();
        if (!token) {
            return next.handle(request);
        }

        const httpRequest = request.clone({
            headers: request.headers
                .set('Authorization', token)
        });

        return next.handle(httpRequest);
    }
}
