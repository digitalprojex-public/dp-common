import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (request.headers.has('Content-Type')) {
            return next.handle(request);
        }

        const httpRequest = request.clone({
            headers: request.headers
                .set('Content-Type', 'application/json; charset=utf-8')
        });

        return next.handle(httpRequest);
    }
}
